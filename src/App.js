import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';
import Login from './pages/Login';
import Admin from './pages/Admin';

import './grid.min.css';

class App extends React.Component {
    render() {
        return (
            <BrowserRouter basename="/~xkremece/avtk_frontend/">
                <Switch>
                    <PublicRoute exact path="/" component={Login}/>
                    <PrivateRoute exact path="/admin" component={Admin}/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;