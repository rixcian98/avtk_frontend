import React from 'react';
import { CSSTransition } from 'react-transition-group';
import moment from 'moment';
import { Doughnut } from 'react-chartjs-2';

import './Stats.scss';

class Stats extends React.Component {

    state = {
        genderChartData: {
            labels: ['Muži', 'Ženy'],
            datasets: [{
                data: [30, 70],
                backgroundColor: ['#2d98da', '#eb3b5a'],
                borderColor: ['#2d98da', '#eb3b5a'],
                hoverBorderColor: ['#2d98da', '#eb3b5a'],
                hoverBackgroundColor: ['#2d98da', '#eb3b5a'],
            }]
        }
    };

    componentDidMount() {
        moment.updateLocale('en', {
            relativeTime : {
                future: "za %s",
                past:   "před %s",
                s  : 'několika vteřinami',
                ss : '%d vteřiny',
                m:  "minutou",
                mm: "%d minutami",
                h:  "hodinou",
                hh: "%d hodinami",
                d:  "dnem",
                dd: "%d dny",
                M:  "měsícem",
                MM: "%d měsíci",
                y:  "rokem",
                yy: "%d roky"
            }
        });
    }

    renderTableData = () => {
        return this.props.data.last_data.map(obj =>
            (
                <tr key={obj.id}>
                    <td>{ obj.min_age } - { obj.max_age }</td>
                    <td>{ obj.gender === 'zena' ? 'Žena' : 'Muž' }</td>
                    <td>{ moment(obj.time).fromNow()  }</td>
                </tr>
            )
        )
    };


    render() {
        return (
            <CSSTransition
                in={this.props.showStats}
                timeout={500}
                classNames="toggle"
                unmountOnExit
                onEnter={() => this.props.toggleStats(true)}
                onExited={() => this.props.toggleStats(false)}
            >
                <div className="container" style={{ width: '100%' }}>
                    <div className="row">
                        <div className="col-6 section">
                            <h4 className="section__heading">Poslední detekce</h4>
                            <table className="datatable">
                                <thead>
                                    <tr>
                                        <th>věk</th>
                                        <th>pohlaví</th>
                                        <th>čas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { this.renderTableData() }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-6 section">
                            <h4 className="section__heading">Rozdělení podle pohlaví</h4>
                            <Doughnut data={this.state.genderChartData}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 section">
                            <h4 className="section__heading">Rozdělení podle věku</h4>
                        </div>
                        <div className="col-6 section">
                            <h4 className="section__heading">Nastavení</h4>
                        </div>
                    </div>
                </div>
            </CSSTransition>
        )
    }
}

export default Stats;