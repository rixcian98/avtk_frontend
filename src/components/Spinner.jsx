import React from 'react';
import { CSSTransition } from 'react-transition-group';

import './Spinner.scss';

export default props => {
    return (
        <CSSTransition
            in={props.showSpinner}
            timeout={500}
            classNames="toggle"
            unmountOnExit
            onEnter={() => props.toggleSpinner(true)}
            onExited={() => props.toggleSpinner(false)}
        >
            <div className="spinner">
                <div className='demo'>
                    <div className='circle'>
                        <div className='inner'></div>
                    </div>
                    <div className='circle'>
                        <div className='inner'></div>
                    </div>
                    <div className='circle'>
                        <div className='inner'></div>
                    </div>
                    <div className='circle'>
                        <div className='inner'></div>
                    </div>
                    <div className='circle'>
                        <div className='inner'></div>
                    </div>
                </div>
            </div>
        </CSSTransition>
    )
}