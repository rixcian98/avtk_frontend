import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

import './Login.scss';

class Login extends React.Component {

    state = {
        username: '',
        password: '',
        buttonText: 'přihlásit',
    };

    componentDidMount() {
        document.title = 'AVTK | Přihlášení';
    }

    handleSubmit = (e) => {
          e.preventDefault();

          if (!this.state.username.length < 1 && !this.state.password.length < 1) {
              this.setState({ buttonText: 'probíhá přihlašování ...' });

              axios.post('https://akela.mendelu.cz/~xkremece/avtk/public/index.php/auth', {
                  username: this.state.username,
                  password: this.state.password
              }).then(res => {
                  //console.log(res.data);
                  localStorage.setItem('avtk_user', JSON.stringify(res.data));
                  setTimeout(() => {
                      this.setState({ buttonText: 'zadali jste správné údaje' })
                  }, 1000);
                  setTimeout(() => {
                      this.setState({ buttonText: 'přesměrování do administrace ...' })
                  }, 2000);
                  setTimeout(() => {
                      this.props.history.push('/admin');
                  }, 3000);
              }).catch(e => {
                  setTimeout(() => {
                      this.setState({ buttonText: 'neplatné údaje' })
                  }, 1000)
              });
          } else {
              this.setState({ buttonText: 'vyplňte všechna pole' });
              setTimeout(() => {
                  this.setState({ buttonText: 'přihlásit' });
              }, 1500);
          }

    };

    render() {
        return (
            <div className="page login">
                <div className="loginform__wrapper">
                    <h1>Přihlášení</h1>

                    <form className="loginform" onSubmit={(e) => this.handleSubmit(e)}>
                        <div className="formcontrol">
                            <label>Uživatelské jméno</label>
                            <input
                                type="text"
                                value={this.state.username}
                                onChange={(e) => this.setState({ username: e.target.value })}
                            />
                        </div>
                        <div className="formcontrol">
                            <label>Heslo</label>
                            <input
                                type="password"
                                onChange={(e) => this.setState({ password: e.target.value })}
                            />
                        </div>
                        <button className="loginform__button">{ this.state.buttonText }</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);