import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Spinner from '../components/Spinner';
import Stats from '../components/Stats';

import './Admin.scss'

class Admin extends React.Component {

    state = {
        logoutButtonText: 'odhlásit',
        showSpinner: true,
        showStats: false,
        data: {
            last_data: [],
            data_count: 0,
            man_count: 0,
            woman_count: 0
        }
    };

    componentDidMount() {
        document.title = 'AVTK | Administrace';
        this.getData();
    }

    getData = () => {
        axios.post('https://akela.mendelu.cz/~xkremece/avtk/public/index.php/alldata', {
            token: JSON.parse(localStorage.getItem('avtk_user')).token
        }).then(res => {
            //console.log(res.data);
            setTimeout(() => this.setState({ showSpinner: false }), 100);
            setTimeout(() => this.setState({ data: res.data }), 650);
            setTimeout(() => this.setState({ showStats: true }), 700);
        }).catch(() => {
            if (localStorage.getItem('avtk_user')) {
                localStorage.removeItem('avtk_user')
            }
            this.props.history.push('/');
        });
    };

    logout = () => {
        localStorage.removeItem('avtk_user');
        this.setState({ logoutButtonText: 'odhlašování ...' });
        setTimeout(() => {
            this.props.history.push('/');
        }, 1000)
    };

    toggleSpinner = val => { this.setState({ showSpinner: val }) };
    toggleStats = val => { this.setState({ showStats: val }) };

    refreshData = () => {
        this.setState({ showStats: false});
        setTimeout(() => this.setState({ data: [], showSpinner: true }), 550);
        setTimeout(() => this.getData(), 600);
    };

    render() {
        return (
            <div className="page admin">
                <header className="admin__header">
                    <button className="button__logout" onClick={() => this.logout()}>{ this.state.logoutButtonText }</button>
                </header>
                <div className="pagebody">
                    <div className="container">
                        <div className="row">
                            <div className="col-6"><h1> Administrace</h1></div>
                            <div className="col-6 button__refresh__wrapper">
                                <button className="button__refresh" onClick={() => this.refreshData()}>
                                    <FontAwesomeIcon className="icon__sync" icon="sync" />
                                    &nbsp; Aktualizovat
                                </button>
                            </div>
                        </div>
                    </div>
                    <Spinner showSpinner={this.state.showSpinner} toggleSpinner={val => this.toggleSpinner(val)}/>
                    <Stats data={this.state.data} showStats={this.state.showStats} toggleStats={val => this.toggleStats(val)}/>
                </div>
            </div>
        )
    }
}

export default withRouter(Admin);